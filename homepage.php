<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="info">
		<div class="wrapper">

			<section id="top">

				<div id="hours">
					<div class="content">
						<h3>Hours</h3>
						<?php the_field('hours'); ?>
					</div>
				</div>

				<div id="location">
					<div class="content">
						<h3>Location</h3>
						<?php the_field('location'); ?>	

						<div class="reservations">
							<?php the_field('reservations'); ?>
						</div>	
					</div>

				</div>

				<div id="menu">
					<div class="content">
						<h3>Menus</h3>

						<div class="menu-wrapper">

							<?php if(have_rows('menus')): while(have_rows('menus')): the_row(); ?>
							 
								<div class="cta">
									<a href="<?php the_sub_field('pdf'); ?>" class="btn" rel="external"><?php the_sub_field('label'); ?></a>
								</div>

							<?php endwhile; endif; ?>

						</div>						
					</div>
				</div>

			</section>
		
			<section id="bottom">

				<div id="hrg">
					<a href="http://www.heavyrestaurantgroup.com/" rel="external">Heavy Restaurant Group</a>					
				</div>

				<div id="note">
					<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>					
				</div>

				<div id="social">

					<div class="links">
						<a href="<?php the_field('facebook'); ?>" rel="external" class="facebook">
							<img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebook" />
						</a>

						<a href="<?php the_field('instagram'); ?>" rel="external" class="instagram">
							<img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram" />
						</a>

						<a href="<?php the_field('twitter'); ?>" rel="external" class="twitter">
							<img src="<?php bloginfo('template_directory') ?>/images/twitter.svg" alt="Twitter" />
						</a>
					</div>
					
				</div>

			</section>	


		</div>
	</section>


<?php get_footer(); ?>