	<footer>
		<div class="wrapper">

			<div class="moniker">
				<img src="<?php $image = get_field('moniker'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</footer>

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

</body>
</html>